
document.addEventListener("DOMContentLoaded", function(event) {


    // Le jeu


    class Jeu {



        constructor(_idSvg, _idPointage){

            console.log("yeah") ;

            this.s =Snap(_idSvg) ;

            this.sortiPointage = document.querySelector(_idPointage) ;

            this.grandeurCarre = 20 ;
            this.grandeurGrille = 15;



        }

        nouvellePartie(){

            this.finPartie();
            this.affichageScore(1) ;

            // this  ( important )

            this.pomme = new Pomme(this);

            this.serpent = new Serpent(this);

        }

        finPartie(){

            if(this.pomme !== undefined){

                this.pomme.supprimePomme();

                this.pomme = undefined ;
            }


            if(this.serpent !==undefined){

                this.serpent.supprimeSerpent();
                this.serpent = undefined ;
            }


        }

        affichageScore(_lePointage){

            this.sortiPointage.innerHTML = _lePointage ;

        }



    }



    // Le serpent


    class Serpent {

         constructor(_leJeu){
            console.log("yeah 2")

             this.leJeu = _leJeu ;

            this.currentX = -1 ;
            this.currentY = 0 ;


            this.nextMoveX = 1 ;
            this.nextMoveY = 0 ;


            this.serpentLongueur = 1 ;
            this.tblCarreSerpent = [];

            this.touche = false;

            this.vitesse = 250 ;

            // bind encore this !!!

            this.timing = setInterval(this.controleSerpent.bind(this) , this.vitesse) ;

            // bind important !!

            document.addEventListener("keydown" , this.verifTouche.bind(this))

        }



        verifTouche(_evt){

           var evt = _evt ;
            // console.log(evt.keyCode);

            this.deplacement(evt.keyCode) ;


        }





        deplacement(dirCode){


             switch(dirCode){

                 case 37 :
                     this.nextMoveX = -1 ;
                     this.nextMoveY = 0 ;
                     break ;

                 case 38 :
                     this.nextMoveX = 0 ;
                     this.nextMoveY = -1 ;
                     break ;

                 case 39 :
                     this.nextMoveX = 1 ;
                     this.nextMoveY = 0 ;
                     break ;

                 case 40 :
                     this.nextMoveX = 0 ;
                     this.nextMoveY = 1 ;
                     break ;



             }

              console.log(this.nextMoveX , this.nextMoveY) ;

        }






        controleSerpent() {

            var nextX = this.currentX + this.nextMoveX;
            var nextY = this.currentY + this.nextMoveY;


            this.tblCarreSerpent.forEach(function (element) {

                if(nextX === element[1] && nextY === element[2]){

                    console.log("toche moi-même")
                    this.leJeu.finPartie();
                    this.touche = true ;

                }

            }.bind(this)) ;





            if (nextY < 0 || nextX < 0 || nextY > this.leJeu.grandeurGrille - 1 || nextX > this.leJeu.grandeurGrille - 1)
            {

                console.log("deadafmate!!")
                this.leJeu.finPartie();
                this.touche = true ;
            }



             if(!this.touche){

                if(this.currentX === this.leJeu.pomme.pomme[1] && this.currentY === this.leJeu.pomme.pomme[2]) {

                    this.serpentLongueur++;
                    this.leJeu.affichageScore(this.serpentLongueur)
                    this.leJeu.pomme.supprimePomme();
                    this.leJeu.pomme.ajoutePomme();
                }

             this.dessineCarre(nextX , nextY);
             this.currentX = nextX ;
             this.currentY = nextY ;

        }

        }


        dessineCarre(x , y){

        // woah


             var unCarre = [this.leJeu.s.rect(x * this.leJeu.grandeurCarre , y * this.leJeu.grandeurCarre , this.leJeu.grandeurCarre , this.leJeu.grandeurCarre) , x , y ]

             this.tblCarreSerpent.push(unCarre) ;

             if(this.tblCarreSerpent.length > this.serpentLongueur){

                 this.tblCarreSerpent[0][0].remove();
                 this.tblCarreSerpent.shift();




             }


        }




        supprimeSerpent(){


             clearInterval(this.timing) ;

             while (this.tblCarreSerpent.length > 0 ){
                 this.tblCarreSerpent[0][0].remove();
                 this.tblCarreSerpent.shift();
             }
        }



    }



    // La pomme



    class Pomme {

        constructor(_leJeu){
            console.log("yeah 3")

            this.leJeu = _leJeu ;

            this.pomme = [];

            this.ajoutePomme();

        }



        ajoutePomme(){

            // o.o

          var posX = Math.floor(Math.random() * this.leJeu.grandeurGrille) ;
          var posY = Math.floor(Math.random() * this.leJeu.grandeurGrille) ;

          // svg dessiner rect et creer tableau

              this.pomme = [this.leJeu.s.rect(posX * this.leJeu.grandeurCarre , posY * this.leJeu.grandeurCarre , this.leJeu.grandeurCarre , this.leJeu.grandeurCarre )
              .attr ({fill: 'red'}) , posX , posY]

        }



        supprimePomme(){

            this.pomme[0].remove();

        }


    }


    var unePartie = new Jeu("#jeu" , "#pointage");

    var btnJouer = document.querySelector("#btnJouer");
    btnJouer.addEventListener("click" , nouvellePartie);


    function nouvellePartie(){

        unePartie.nouvellePartie() ;

    }













});